@extends('layouts.master')

@section('title')
    List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success btn-sm my-2">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
          <tr>
              <td>{{$key+1}}</td>
              <td>{{$item->nama}}</td>
              <td>{{$item->umur}}</td>
              <td>{{$item->bio}}</td>
              <td>
                  <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                  <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                  <form action="/cast/{{$item->id}}" method ="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                  </form>

              </td>
          </tr>
      @empty
      <tr>
          <td>Tidak ada Data</td>
      </tr>
      @endforelse
    </tbody>
  </table>
@endsection