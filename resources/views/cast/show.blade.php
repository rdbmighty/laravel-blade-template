@extends('layouts.master')

@section('title')
    Cast Detail {{$cast->nama}}
@endsection

@section('content')
    <h1>{{$cast->nama}}</h1>
    <h2>{{$cast->umur}}</h2>
    <p>{{$cast->bio}}</p>
@endsection